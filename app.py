from handlers import (TwitterHandler,
                      FacebookPostHandler,
                      YouTubeHandler)
from json import load
import pandas as pd
import logging


class Parser:

    SERVICES = {
        'twitter': TwitterHandler,
        'facebook': FacebookPostHandler,
        'youtube': YouTubeHandler
    }

    def __init__(self, persons):
        self._persons = persons
        self._handlers = {}
        self._dataset = {
            'person': [],
            'phrase': [],
            'source': []
        }

    def __prepare__handlers(self, payload):
        handlers = []
        for source_name, payload in payload.items():
            if source_name in self.SERVICES:
                handlers.append(self.SERVICES[source_name](payload))
        return handlers

    def collect(self):
        for person_name, payload in self._persons.items():
            for handler in self.__prepare__handlers(payload):
                phrases = handler.load()
                if len(phrases):
                    self._dataset['person'].extend([ person_name ] * len(phrases))
                    self._dataset['source'].extend([ handler.name ] * len(phrases))
                    self._dataset['phrase'].extend(phrases)
                    logging.info((person_name, handler.name,  len(phrases)))
        self._dataset = pd.DataFrame.from_dict(self._dataset)

    @property
    def dataset(self):
        return self._dataset


with open('request.json') as f:
    parser = Parser(persons=load(f))
    parser.collect()
    parser.dataset.to_csv('dataset.csv')

