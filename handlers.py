from youtube_transcript_api import YouTubeTranscriptApi
from youtube_transcript_api._errors import TranscriptsDisabled, NoTranscriptFound
import re
import requests
from config import (FB_FEED_URL,
                    FB_ACCESS_TOKEN,
                    FB_MAX_POSTS)
import logging

class Handler:

    @property
    def name(self):
        return self.__class__.__name__.replace('Handler', '')

    def load(self):
        raise NotImplemented

class YouTubeHandler(Handler):

    def __init__(self, video_slugs):
        self._video_slugs = video_slugs
        self._phrases = []
        self._ignore = re.compile('(\[.*?\])|(\(.*?\))')

    def load(self):
        for slug in self._video_slugs:
            response = []
            try:
                response = YouTubeTranscriptApi.get_transcript(slug, languages=['en'])
            except (TranscriptsDisabled, NoTranscriptFound):
                logging.error(f'Can`t get transcript for: {slug}')
            for item in response:
                if not re.match(self._ignore, item['text']):
                    self._phrases.append(item['text'])
        return self._phrases


class TwitterHandler(Handler):

    def __init__(self, user_name):
        self._user_name = user_name

    def load(self):
        return []


class FacebookPostHandler(Handler):

    def __init__(self, user_name):
        self._user_name = user_name
        self._posts = []

    def load(self, url=None):
        r = requests.get(url or FB_FEED_URL.format(self._user_name, FB_ACCESS_TOKEN))
        data = r.json()
        if 'error' in data:
            logging.error(data['error']['message'])
            return []
        for post in data['data']:
            self._posts.append(post.get('message'))
        next_page = data.get('paging', {}).get('next')
        if next_page is not None:
            return self.load(next_page)

